package start.createtablesandfilldata;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

@Resource
public class ReadCSVFile {

	String[][] elmntsCSV;
	
	public String[][] readCSV(String filePath){
		
		List<String> fileCSV = new ArrayList<>();

		try {
			fileCSV = Files.readAllLines(Paths.get(filePath));
			String[][] elementsCSV = new String[fileCSV.size()][1];
			
			for (int i = 0; i < fileCSV.size(); i++) {
				String anElement = fileCSV.get(i);
				String[] elements = anElement.split(",");
				elementsCSV[i] = elements;
			}
			
			elmntsCSV = elementsCSV;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return elmntsCSV;
	}
}
