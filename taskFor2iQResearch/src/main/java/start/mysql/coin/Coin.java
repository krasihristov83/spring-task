package start.mysql.coin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "coin")
public class Coin {

	@Id
	@Column(name="ID")
	public Integer id;
	
	@Column(name="coinName")
	private String coinName;
	
	@Column(name="description")
	public String description;
	
	@Column(name="Symbol")
	private String symbol;
	
	public Coin() {
		
	}
	
	public Coin(int id, String coinName, String description, String symbol) {
		super();
		this.id = id;
		this.coinName = coinName;
		this.description = description;
		this.symbol = symbol;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCoinName() {
		return coinName;
	}

	public void setCoinName(String coinName) {
		this.coinName = coinName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
}
