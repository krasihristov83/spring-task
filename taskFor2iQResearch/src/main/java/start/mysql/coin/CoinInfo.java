package start.mysql.coin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pricedata_hist")
public class CoinInfo {

	@Id
	@Column(name="ID")
	public Integer id;
	
	@Column(name="refID_coin")
	public Integer coinId;
	
	@Column(name="openPrice")
	public Float openprice;
	
	@Column(name="closePrice")
	public Float closeprice;
	
	@Column(name="highPrice")
	public Float highprice;
	
	@Column(name="lowPrice")
	public Float lowprice;
	
	public CoinInfo() {
		
	}

	public CoinInfo(Integer id, Integer coinId, Float openprice, Float closeprice, Float highprice, Float lowprice) {
		super();
		this.id = id;
		this.coinId = coinId;
		this.openprice = openprice;
		this.closeprice = closeprice;
		this.highprice = highprice;
		this.lowprice = lowprice;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCoinId() {
		return coinId;
	}

	public void setCoinId(Integer coinId) {
		this.coinId = coinId;
	}

	public Float getOpenprice() {
		return openprice;
	}

	public void setOpenprice(Float openprice) {
		this.openprice = openprice;
	}

	public Float getCloseprice() {
		return closeprice;
	}

	public void setCloseprice(Float closeprice) {
		this.closeprice = closeprice;
	}

	public Float getHighprice() {
		return highprice;
	}

	public void setHighprice(Float highprice) {
		this.highprice = highprice;
	}

	public Float getLowprice() {
		return lowprice;
	}

	public void setLowprice(Float lowprice) {
		this.lowprice = lowprice;
	}
}
