package start.mysql.coin;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoinInfoRepository extends JpaRepository<CoinInfo, Integer> {

	List<CoinInfo> findAllByCoinId(int id);
}
