package start.mysql.coin;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import start.createtablesandfilldata.ReadCSVFile;

@Service
public class CoinService {
	
	ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
	ReadCSVFile obj = (ReadCSVFile) context.getBean("readCSVFile");
	
	@Autowired
	private CoinRepository coinRepository;
	
	@Autowired
	private CoinInfoRepository coinInfoRepository;

	public List<Coin> getCoinList(){
		List<Coin> coinList = new ArrayList<>();
		
		coinRepository.findAll()
		.forEach(coinList::add);
		
		return coinList;
	}
	
	public List<CoinInfo> getCoinInfo(int id){
		
		List<CoinInfo> coinList = new ArrayList<>();
		
		coinInfoRepository.findAllByCoinId(id)
		.forEach(coinList::add);
		
		return coinList;
	}
	
	private void fillDataInCoinTable(){
		String filePath = "src\\main\\resources\\data\\coin.csv";
		String[][] elmntsCSV = obj.readCSV(filePath);
		
		
		List<Coin> listCoins = new ArrayList<>();
		String[] temp = new String[4];
		
		for (int i = 1; i < elmntsCSV.length; i++) {
			temp = elmntsCSV[i];
			Coin coin = new Coin();
			
			for (int j = 0; j < temp.length; j++) {
				
				switch (j) {
				case 0:
					coin.setId(Integer.valueOf(temp[j]));
					break;
				case 1:
					coin.setCoinName(temp[j]);
					break;
				case 2:
					coin.setDescription(temp[j]);
					break;
				case 3:
					coin.setSymbol(temp[j]);
					listCoins.add(coin);
					break;

				default:
					break;
				}
			}
		}
		
		for (Coin coins : listCoins) {
			coinRepository.save(coins);
		}
	}
	
	private void fillDataInHistoryTable(){
		String filePath = "src\\main\\resources\\data\\history.csv";
		String[][] elmntsCSV = obj.readCSV(filePath);
		
		
		List<CoinInfo> listCoins = new ArrayList<>();
		String[] temp = new String[6];
		
		for (int i = 1; i < elmntsCSV.length; i++) {
			temp = elmntsCSV[i];
			CoinInfo coinInfo = new CoinInfo();
			
			for (int j = 0; j < temp.length; j++) {
				
				switch (j) {
				case 0:
					coinInfo.setId(Integer.valueOf(temp[j]));
					break;
				case 1:
					coinInfo.setCoinId(Integer.valueOf(temp[j]));
					break;
				case 2:
					coinInfo.setOpenprice(Float.valueOf(temp[j]));
					break;
				case 3:
					coinInfo.setCloseprice(Float.valueOf(temp[j]));
					break;
				case 4:
					coinInfo.setHighprice(Float.valueOf(temp[j]));
					break;
				case 5:
					coinInfo.setLowprice(Float.valueOf(temp[j]));
					listCoins.add(coinInfo);
					break;

				default:
					break;
				}
			}
		}
		
		for (CoinInfo coinInfo : listCoins) {
			coinInfoRepository.save(coinInfo);
		}
	}
	

	public void setDatabase() {

		fillDataInCoinTable();
		fillDataInHistoryTable();
	}
}
