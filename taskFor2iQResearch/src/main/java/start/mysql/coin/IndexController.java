package start.mysql.coin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
	
	private boolean isDataReady = false;
	
	@Autowired
	private CoinService coinService;
	
	@RequestMapping("/coins") 
	public String getCoinList(Model model){
		
		if(!isDataReady){
			coinService.setDatabase();
			isDataReady = true;
		}
		
		model.addAttribute("coins", coinService.getCoinList());
		return "coins";
	}
	
	@RequestMapping("/coins/{id}")
	public String getCoinInfo(@PathVariable int id, Model model){
		model.addAttribute("info", coinService.getCoinInfo(id));
		return "info";
	}
}
